# Koewolucyjna optymalizacja trójwymiarowych problemów komiwojażera

Celem projektu jest opracowanie podejścia koewolucyjnego w problemach optymalizacji trójwymiarowego problemu komiwojażera.
W zakresie prac projektowych należy stworzyć aplikację umożliwiającą zbadanie wpływu koewolucji oraz korzyści wynikających z zainicjowania populacji początkowej za pomocą algorytmu optymalizacji lokalnej. Rozważane zadanie optymalizacji zdefiniowane jest dla kryteriów związanych z minimalizacją długości trasy w przestrzeni trójwymiarowej przy jednoczesnej obecności typowych ograniczeń dla problemu komiwojażera.

**Projekt w trakcie realizacji**
