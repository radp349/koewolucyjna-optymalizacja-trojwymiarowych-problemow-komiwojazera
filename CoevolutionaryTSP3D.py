import sys
from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QLineEdit, QCheckBox, QComboBox, QFileDialog, QVBoxLayout, QButtonGroup, QRadioButton, QWidget, QPushButton, QHBoxLayout, QSpinBox, QDoubleSpinBox, QLabel, QSizePolicy
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.animation as animation
from matplotlib import cm
from functools import partial
import numpy as np
import random
import math as m
# rozmiar populacji 500
# mutacja z inwersją przedziału i przesunięciem
# ?zrobić jakiś problem 50, 300?

class MyWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setGeometry(200, 200, 1400, 700)
        self.setWindowTitle("CoevolutionaryTSP3D")
        self.delay = 1 #ms opóźnienia animacji
        self.ile_populacji = 1
        self.rozmiar_populacji = 120
        self.rozmiar_potomstwa = 180
        self.alfaomega = True #Czy można przestawić pierwszą i ostatnią gwiazdę?
        self.sukcecja_calosciowa = True
        self.Populacje = []
        self.Potomstwa = []
        self.Oceny = []
        self.Gwiazdy= []
        self.Konstelacje = []
        self.p_krzyzowania = 0.75
        self.p_mutacji = 0.5
        self.ilu_rywali = 5
        # Przechowywanie najlepszego rozwiązania
        self.najlepsze = None
        self.najlepszy_dystans = m.inf
        self.sredni_dystans = m.inf
        self.najlepszy_narodziny = 0
        self.epoka = 0
        self.epoka_info = None
        self.path_length_info = None
        
        self.ile_gwiazd_label = QLabel("Liczba Losowych Gwiazd:", self)  # Etykieta dla pola tekstowego
        self.ile_gwiazd_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.ile_gwiazd_box = QSpinBox(self) # Używamy QSpinBox do pobierania wartości liczbowych
        self.ile_gwiazd_box.setValue(100)
        self.ile_gwiazd_box.setMinimum(2)
        self.ile_gwiazd_box.setMaximum(1000)
        
        
        self.load_button = QRadioButton("Wczytaj Gwiazdy", self)
        self.random_button = QRadioButton("Losuj Gwiazdy", self)
        self.load_button.setChecked(True)  # Domyslnie wybieramy losowanie gwiazd
        self.radio_group = QButtonGroup(self)
        self.radio_group.addButton(self.load_button)
        self.radio_group.addButton(self.random_button)
        
        self.file_path = QLineEdit(self)
        self.file_path.setText('benchmark100_1k/star100_xyz.txt')

        self.co_evolution_label = QLabel("Ustawienia Koewolucji:", self)  # Etykieta dla ustawienia Koewolucji
        self.co_evolution_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.none_button = QRadioButton("Brak", self)
        self.competition_button = QRadioButton("Rywalizacja", self)
        self.cooperation_button = QRadioButton("Kooperacja", self)
        self.none_button.setChecked(True)  # Domyslnie wybieramy brak koewolucji
        self.co_evolution_group = QButtonGroup(self)
        self.co_evolution_group.addButton(self.none_button)
        self.co_evolution_group.addButton(self.competition_button)
        self.co_evolution_group.addButton(self.cooperation_button)
        
        self.ilu_rywali_label = QLabel("Liczba rywalizujących populacji:", self)  
        self.ilu_rywali_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.ilu_rywali_box = QSpinBox(self) 
        self.ilu_rywali_box.setValue(self.ilu_rywali)
        self.ilu_rywali_box.setMinimum(2)
        self.ilu_rywali_box.setMaximum(10)
        
        self.nearest_neighbour_label = QLabel("Sposób Inicjalizacji Populacji Początkowej:", self)  
        self.nearest_neighbour_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.random_init_button = QRadioButton("Inicjalizacja Losowa", self)
        self.nn_init_button = QRadioButton("Algorytm Najbliższego Sąsiada", self)
        self.random_init_button.setChecked(True) 
        self.nearest_neighbour_group = QButtonGroup(self)
        self.nearest_neighbour_group.addButton(self.random_init_button)
        self.nearest_neighbour_group.addButton(self.nn_init_button)
        
        self.selection_label = QLabel("Metoda selekcji:", self)  
        self.selection_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.roulette_only_button = QRadioButton("K. Ruletki", self)
        self.tournament_only_button = QRadioButton("Turniejowa", self)
        self.selection_switch_button = QRadioButton("Switch", self)
        self.selection_random_button = QRadioButton("Losowo", self)
        self.selection_switch_button.setChecked(True) 
        self.selection_group = QButtonGroup(self)
        self.selection_group.addButton(self.roulette_only_button)
        self.selection_group.addButton(self.tournament_only_button)
        self.selection_group.addButton(self.selection_switch_button)
        self.selection_group.addButton(self.selection_random_button)  
        
        self.succession_label = QLabel("Strategia sukcesji:", self)  
        self.succession_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.full_succession_button = QRadioButton("Sukcesja całościowa", self)
        self.partial_succession_button = QRadioButton("Sukcesja częściowa", self)
        self.partial_succession_button.setChecked(True) 
        self.succession_group = QButtonGroup(self)
        self.succession_group.addButton(self.full_succession_button)
        self.succession_group.addButton(self.partial_succession_button)

        self.rozmiar_populacji_label = QLabel("Rozmiar populacji:    ", self)  
        self.rozmiar_populacji_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.rozmiar_populacji_label.setFixedWidth(100)
        self.rozmiar_populacji_box = QSpinBox(self) 
        self.rozmiar_populacji_box.setValue(self.rozmiar_populacji)
        self.rozmiar_populacji_box.setMinimum(2)
        self.rozmiar_populacji_box.setMaximum(1000)
        
        self.rozmiar_potomstwa_label = QLabel("Rozmiar potomstwa:", self)  
        self.rozmiar_potomstwa_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.rozmiar_potomstwa_label.setFixedWidth(100)
        self.rozmiar_potomstwa_box = QSpinBox(self) 
        self.rozmiar_potomstwa_box.setValue(self.rozmiar_potomstwa)
        self.rozmiar_potomstwa_box.setMinimum(2)
        self.rozmiar_potomstwa_box.setMaximum(2000)

        self.operators_label = QLabel("Ustawienia Krzyżowania i Mutacji:", self)  
        self.operators_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        
        self.crossover_chance_label = QLabel("Prawdopodobieństwo Krzyżowania:", self)  
        self.crossover_chance_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.crossover_chance_label.setFixedWidth(170)
        self.crossover_chance_box = QDoubleSpinBox(self) 
        self.crossover_chance_box.setValue(self.p_krzyzowania)  
        self.crossover_chance_box.setMinimum(0) 
        self.crossover_chance_box.setMaximum(1)
        self.crossover_chance_box.setSingleStep(0.01) 
        
        self.cx_only_button = QRadioButton("Tylko CX", self)
        self.pmx_only_button = QRadioButton("Tylko PMX", self)
        self.crossover_switch_button = QRadioButton("Switch", self)
        self.crossover_random_button = QRadioButton("Losowo", self)
        self.crossover_switch_button.setChecked(True) 
        self.crossover_group = QButtonGroup(self)
        self.crossover_group.addButton(self.cx_only_button)
        self.crossover_group.addButton(self.pmx_only_button)
        self.crossover_group.addButton(self.crossover_switch_button)
        self.crossover_group.addButton(self.crossover_random_button)  
        
        self.mutation_chance_label = QLabel("Prawdopodobieństwo Mutacji:", self)  
        self.mutation_chance_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.mutation_chance_label.setFixedWidth(170)
        self.mutation_chance_box = QDoubleSpinBox(self) 
        self.mutation_chance_box.setValue(self.p_mutacji)  
        self.mutation_chance_box.setMinimum(0) 
        self.mutation_chance_box.setMaximum(1)
        self.mutation_chance_box.setSingleStep(0.01)
        
        self.mutation_combo_box = QComboBox(self)
        self.mutation_combo_box.addItem("Mutacja dostosowana do stopnia zaawansowania rozwiązania")
        self.mutation_combo_box.addItem("Wszystkie metody mutacji losowo")
        self.mutation_combo_box.addItem("Wszystkie metody mutacji na zmianę")
        self.mutation_combo_box.addItem("Tylko mutacja przez wymieszanie przedziału")
        self.mutation_combo_box.addItem("Tylko mutacja przez zamianę pary gwiazd")   
        self.mutation_combo_box.addItem("Tylko mutacja przez inwersję i przesunięcie przedziału") 
        self.mutation_combo_box.addItem("Tylko mutacja przez przestawienie jednej gwiazdy")    
        self.mutation_combo_box.addItem("Tylko mutacja przez inwersję przedziału")
        self.mutation_combo_box.setCurrentIndex(0)      
        
        self.iterations_label = QLabel("X =", self)  
        self.iterations_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.iterations_box = QSpinBox(self) 
        self.iterations_box.setValue(50)
        self.iterations_box.setMinimum(1)
        self.iterations_box.setMaximum(100)

        self.fig = plt.figure(figsize=(16, 8))
        self.ax = self.fig.add_subplot(121, projection='3d')
        self.ax2 = self.fig.add_subplot(122)  # Dodajemy drugi wykres 2D na pozycji 122
        self.ax2.set_xlabel('Epoka')
        self.ax2.set_ylabel('Długość trasy')
        
        plt.subplots_adjust(left=0.01, right=0.99, bottom=0.2, top=0.8, wspace=0.2)
        self.epochs = []
        self.path_lengths = []


        self.canvas = FigureCanvas(self.fig)
        self.canvas.setMinimumSize(400, 400)

        # Przyciski do rozpoczęcia, zatrzymania, wznowienia i zakończenia animacji
        self.start_button = QPushButton('Start', self)
        self.start_button.clicked.connect(self.start_animation)
        self.browse_button = QPushButton("Browse...", self)
        self.browse_button.clicked.connect(self.browse_files)
        self.stop_button = QPushButton('Stop', self)
        self.stop_button.clicked.connect(self.stop_animation)
        self.stop_button.setEnabled(False)
        self.save_button = QPushButton('Zapisz Bieżące Rozwiązanie', self)
        self.save_button.clicked.connect(self.Zapis)
        self.save_button.setEnabled(False)
        self.resume_button = QPushButton('Resume', self)
        self.resume_button.clicked.connect(self.resume_animation)
        self.resume_button.setEnabled(False)
        self.iterations_button = QPushButton('Wykonaj X iteracji', self)
        self.iterations_button.clicked.connect(self.X_iteracji)
        self.iterations_button.setEnabled(False)
        self.end_button = QPushButton('End', self)
        self.end_button.clicked.connect(self.end_animation)
        self.end_button.setEnabled(False)


        # Używamy QVBoxLayout dla przycisków
        self.button_layout = QVBoxLayout()
        self.button_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.button_layout.setSpacing(5)
        self.button_layout.setAlignment(Qt.AlignHCenter)
        
        self.ile_gwiazd_layout = QHBoxLayout()
        self.ile_gwiazd_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.ile_gwiazd_layout.setSpacing(5)  
        self.ile_gwiazd_layout.addWidget(self.ile_gwiazd_label)
        self.ile_gwiazd_layout.addWidget(self.ile_gwiazd_box)
    
        
        self.radio_layout = QHBoxLayout()
        self.radio_layout.addWidget(self.load_button)
        self.radio_layout.addWidget(self.random_button)
        
        self.browse_layout = QHBoxLayout()
        self.browse_layout.addWidget(self.browse_button)
        self.browse_layout.addWidget(self.file_path)
        
        self.co_evolution_center_label_layout = QHBoxLayout()
        self.co_evolution_center_label_layout.addWidget(self.co_evolution_label)
        
        self.co_evolution_layout = QHBoxLayout()
        self.co_evolution_layout.addWidget(self.none_button)
        self.co_evolution_layout.addWidget(self.competition_button)
        self.co_evolution_layout.addWidget(self.cooperation_button)
        
        self.ilu_rywali_layout = QHBoxLayout()
        self.ilu_rywali_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.ilu_rywali_layout.setSpacing(5)  
        self.ilu_rywali_layout.addWidget(self.ilu_rywali_label)
        self.ilu_rywali_layout.addWidget(self.ilu_rywali_box)
        
        self.nearest_neighbour_label_layout = QHBoxLayout()
        self.nearest_neighbour_label_layout.setContentsMargins(0, 10, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.nearest_neighbour_label_layout.addWidget(self.nearest_neighbour_label)
        
        self.nearest_neighbour_layout = QHBoxLayout()
        self.nearest_neighbour_layout.addWidget(self.random_init_button)
        self.nearest_neighbour_layout.addWidget(self.nn_init_button)
        
        self.selection_label_layout = QHBoxLayout()
        self.selection_label_layout.setContentsMargins(0, 10, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.selection_label_layout.addWidget(self.selection_label)
        
        self.selection_layout = QHBoxLayout()
        self.selection_layout.addWidget(self.roulette_only_button)
        self.selection_layout.addWidget(self.tournament_only_button)
        self.selection_layout.addWidget(self.selection_switch_button)
        self.selection_layout.addWidget(self.selection_random_button)  
             
        self.succession_label_layout = QHBoxLayout()
        self.succession_label_layout.setContentsMargins(0, 10, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.succession_label_layout.addWidget(self.succession_label)
        
        self.succession_layout = QHBoxLayout()
        self.succession_layout.addWidget(self.full_succession_button)
        self.succession_layout.addWidget(self.partial_succession_button)
        
        self.rozmiar_populacji_layout = QHBoxLayout()
        self.rozmiar_populacji_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.rozmiar_populacji_layout.setSpacing(5)  
        self.rozmiar_populacji_layout.addWidget(self.rozmiar_populacji_label)
        self.rozmiar_populacji_layout.addWidget(self.rozmiar_populacji_box)
        
        self.rozmiar_potomstwa_layout = QHBoxLayout()
        self.rozmiar_potomstwa_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.rozmiar_potomstwa_layout.setSpacing(5)  
        self.rozmiar_potomstwa_layout.addWidget(self.rozmiar_potomstwa_label)
        self.rozmiar_potomstwa_layout.addWidget(self.rozmiar_potomstwa_box)
        
        self.operators_label_layout = QHBoxLayout()
        self.operators_label_layout.setContentsMargins(0, 10, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.operators_label_layout.addWidget(self.operators_label)     
        
        self.crossover_chance_layout = QHBoxLayout()
        self.crossover_chance_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.crossover_chance_layout.setSpacing(5)  
        self.crossover_chance_layout.addWidget(self.crossover_chance_label)
        self.crossover_chance_layout.addWidget(self.crossover_chance_box)
        
        self.crossover_layout = QHBoxLayout()
        self.crossover_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.crossover_layout.setSpacing(5)  
        self.crossover_layout.addWidget(self.cx_only_button)
        self.crossover_layout.addWidget(self.pmx_only_button)
        self.crossover_layout.addWidget(self.crossover_switch_button)
        self.crossover_layout.addWidget(self.crossover_random_button)
                
        self.mutation_chance_layout = QHBoxLayout()
        self.mutation_chance_layout.setContentsMargins(0, 0, 0, 0)  # Ustawiamy marginesy na 0 L/G/P/D
        self.mutation_chance_layout.setSpacing(5)  
        self.mutation_chance_layout.addWidget(self.mutation_chance_label)
        self.mutation_chance_layout.addWidget(self.mutation_chance_box)
        
        self.mutation_layout = QHBoxLayout()
        self.mutation_layout.addWidget(self.mutation_combo_box)
              
        self.iterations_layout = QHBoxLayout()
        self.iterations_layout.setContentsMargins(0, 10, 0, 0)
        self.iterations_layout.addWidget(self.iterations_button)
        self.iterations_layout.addWidget(self.iterations_label)
        self.iterations_layout.addWidget(self.iterations_box)
        
        self.button_layout.addWidget(self.start_button)
        self.button_layout.addLayout(self.ile_gwiazd_layout)
        self.button_layout.addLayout(self.radio_layout)
        self.button_layout.addLayout(self.browse_layout)
        self.button_layout.addLayout(self.co_evolution_center_label_layout)
        self.button_layout.addLayout(self.co_evolution_layout)
        self.button_layout.addLayout(self.ilu_rywali_layout)
        self.button_layout.addLayout(self.nearest_neighbour_label_layout)
        self.button_layout.addLayout(self.nearest_neighbour_layout)
        self.button_layout.addLayout(self.selection_label_layout)
        self.button_layout.addLayout(self.selection_layout)        
        self.button_layout.addLayout(self.succession_label_layout)
        self.button_layout.addLayout(self.succession_layout)
        self.button_layout.addLayout(self.rozmiar_populacji_layout)
        self.button_layout.addLayout(self.rozmiar_potomstwa_layout)
        self.button_layout.addLayout(self.operators_label_layout)
        self.button_layout.addLayout(self.crossover_chance_layout)
        self.button_layout.addLayout(self.crossover_layout)
        self.button_layout.addLayout(self.mutation_chance_layout)
        self.button_layout.addLayout(self.mutation_layout)
        self.button_layout.addLayout(self.iterations_layout)
        self.button_layout.addWidget(self.stop_button)
        self.button_layout.addWidget(self.save_button)
        self.button_layout.addWidget(self.resume_button)
        self.button_layout.addWidget(self.end_button)
        self.button_layout.addStretch(1)
        
        self.layout = QHBoxLayout()
        self.layout.addLayout(self.button_layout) # Dodajemy przyciski do layoutu
        self.layout.addWidget(self.canvas)

        self.centralny_widget = QWidget()
        self.centralny_widget.setLayout(self.layout)
        self.setCentralWidget(self.centralny_widget)

        self.animacja = None

        self.punkty = []
        self.linie = []

        self.licznik = QTimer()
        self.licznik.timeout.connect(self.canvas.draw)

    # Metody do rozpoczęcia, zatrzymania, wznowienia i zakończenia animacji
    def start_animation(self):
        self.ile_gwiazd_box.setEnabled(False)
        self.load_button.setEnabled(False)
        self.random_button.setEnabled(False)
        self.start_button.setEnabled(False)
        self.none_button.setEnabled(False)
        self.competition_button.setEnabled(False)
        self.cooperation_button.setEnabled(False)
        self.ilu_rywali_box.setEnabled(False)
        self.random_init_button.setEnabled(False)
        self.nn_init_button.setEnabled(False)
        self.roulette_only_button.setEnabled(False)
        self.tournament_only_button.setEnabled(False)
        self.selection_switch_button.setEnabled(False)
        self.selection_random_button.setEnabled(False)
        self.full_succession_button.setEnabled(False)
        self.partial_succession_button.setEnabled(False)
        self.rozmiar_populacji_box.setEnabled(False)
        self.rozmiar_potomstwa_box.setEnabled(False)
        self.crossover_chance_box.setEnabled(False)
        self.mutation_chance_box.setEnabled(False)
        self.cx_only_button.setEnabled(False)
        self.pmx_only_button.setEnabled(False)
        self.crossover_switch_button.setEnabled(False)
        self.crossover_random_button.setEnabled(False)
        self.iterations_button.setEnabled(True)
        self.stop_button.setEnabled(True)
        self.save_button.setEnabled(True)
        self.resume_button.setEnabled(False)
        self.end_button.setEnabled(True)
        if not self.animacja:
            self.Gwiazdy.clear()
            if self.random_button.isChecked():  # Jeżeli wybrano losowanie gwiazd
                self.Losuj_Gwiazdy(self.ile_gwiazd_box.value(),-100,100)
            elif self.load_button.isChecked():  # Jeżeli wybrano wczytywanie gwiazd
                self.Wczytaj_Gwiazdy()
                
            self.rozmiar_populacji = self.rozmiar_populacji_box.value()
            if self.full_succession_button.isChecked():  
                self.sukcecja_calosciowa = True
                self.rozmiar_potomstwa = self.rozmiar_populacji_box.value()
            elif self.partial_succession_button.isChecked():
                self.sukcecja_calosciowa = False
                self.rozmiar_potomstwa = self.rozmiar_potomstwa_box.value() 
                
            self.p_krzyzowania = self.crossover_chance_box.value()
            self.p_mutacji = self.mutation_chance_box.value()    

            if self.none_button.isChecked():
                self.ile_populacji = 1
                self.alfaomega = True
                self.Inicjalizacja_Gwiazdami()
                self.Przystosowanie()
                self.Sortuj_Populacje()
                self.najlepsze = self.Gwiazdy.copy()
                self.najlepszy_dystans = sum(m.sqrt((self.najlepsze[k-1][0]-self.najlepsze[k][0])**2 + (self.najlepsze[k-1][1]-self.najlepsze[k][1])**2 + (self.najlepsze[k-1][2]-self.najlepsze[k][2])**2) for k in range(len(self.najlepsze)))
                self.najlepszy_narodziny = 0
                self.animacja = animation.FuncAnimation(self.fig, lambda frame_number: self.main_brak(), init_func=self.Rysuj_Gwiazdy_brak, frames=999999, interval=self.delay, blit=False)
            elif self.competition_button.isChecked():
                self.ile_populacji = self.ilu_rywali_box.value()
                self.alfaomega = True
                self.Inicjalizacja_Gwiazdami()
                self.Przystosowanie()
                self.Sortuj_Populacje()
                self.najlepsze = self.Gwiazdy.copy()
                self.najlepszy_dystans = sum(m.sqrt((self.najlepsze[k-1][0]-self.najlepsze[k][0])**2 + (self.najlepsze[k-1][1]-self.najlepsze[k][1])**2 + (self.najlepsze[k-1][2]-self.najlepsze[k][2])**2) for k in range(len(self.najlepsze)))
                self.najlepszy_narodziny = 0
                self.animacja = animation.FuncAnimation(self.fig, lambda frame_number: self.main_rywalizacja(), init_func=self.Rysuj_Gwiazdy_rywalizacja, frames=999999, interval=self.delay, blit=False)
            elif self.cooperation_button.isChecked():
                self.Podziel_Gwiazdy()
                self.Najblizsze_Gwiazdy()
                self.ile_populacji = len(self.Konstelacje)
                self.alfaomega = False
                self.Inicjalizacja_Konstelacjami()
                self.Przystosowanie()
                self.Sortuj_Populacje()
                self.najlepsze = self.Konstelacje.copy()
                flattened_konstelacje = [element for sublist in self.najlepsze for element in sublist]
                self.najlepszy_dystans = sum(m.sqrt((flattened_konstelacje[k-1][0]-flattened_konstelacje[k][0])**2 + (flattened_konstelacje[k-1][1]-flattened_konstelacje[k][1])**2 + (flattened_konstelacje[k-1][2]-flattened_konstelacje[k][2])**2) for k in range(len(flattened_konstelacje)))
                self.najlepszy_narodziny = 0
                self.animacja = animation.FuncAnimation(self.fig, lambda frame_number: self.main_kooperacja(), init_func=self.Rysuj_Gwiazdy_kooperacja, frames=999999, interval=self.delay, blit=False)
            
            
            self.licznik.start(10)
            self.start_button.setEnabled(False) # Blokujemy przycisk Start

    def stop_animation(self):
        if self.animacja:
            self.animacja.event_source.stop()
            self.stop_button.setEnabled(False)
            self.resume_button.setEnabled(True)

    def resume_animation(self):
        if self.animacja:
            self.animacja.event_source.start()
            self.stop_button.setEnabled(True)
            self.resume_button.setEnabled(False)

    def end_animation(self):
        if self.animacja:
            self.animacja.event_source.stop()
            self.animacja = None
            self.ile_gwiazd_box.setEnabled(True)
            self.load_button.setEnabled(True)
            self.random_button.setEnabled(True)
            self.start_button.setEnabled(True)
            self.none_button.setEnabled(True)
            self.competition_button.setEnabled(True)
            self.cooperation_button.setEnabled(True)
            self.ilu_rywali_box.setEnabled(True)
            self.random_init_button.setEnabled(True)
            self.nn_init_button.setEnabled(True)
            self.roulette_only_button.setEnabled(True)
            self.tournament_only_button.setEnabled(True)
            self.selection_switch_button.setEnabled(True)
            self.selection_random_button.setEnabled(True)
            self.full_succession_button.setEnabled(True)
            self.partial_succession_button.setEnabled(True)
            self.rozmiar_populacji_box.setEnabled(True)
            self.rozmiar_potomstwa_box.setEnabled(True)
            self.crossover_chance_box.setEnabled(True)
            self.mutation_chance_box.setEnabled(True)
            self.cx_only_button.setEnabled(True)
            self.pmx_only_button.setEnabled(True)
            self.crossover_switch_button.setEnabled(True)
            self.crossover_random_button.setEnabled(True)
            self.stop_button.setEnabled(False)
            self.save_button.setEnabled(False)
            self.iterations_button.setEnabled(False)
            self.resume_button.setEnabled(False)
            self.end_button.setEnabled(False)
            self.epoka = 0

            # Usuwamy punkty i linie z wykresu
            self.punkty = []
            self.linie = []

            # Czyścimy wykres i wymuszamy jego przerysowanie
            self.ax.cla()
            self.ax2.cla()
            self.ax2.set_xlabel('Epoka')
            self.ax2.set_ylabel('Długość trasy')
            self.canvas.draw()

    def browse_files(self):
        # Otwieramy okno dialogowe do wyboru pliku
        file_name, _ = QFileDialog.getOpenFileName(self, "Select file", "", "Text Files (*.txt)")
        
        # Jeżeli użytkownik wybrał jakiś plik, ustawiamy ścieżkę do tego pliku w polu do wpisania ścieżki
        if file_name:
            self.file_path.setText(file_name)
            
    
        
    def Wczytaj_Gwiazdy(self):
        self.Gwiazdy.clear() 
        with open(self.file_path.text()) as data:
            for line in data:
                cell = line.split()
                cell[0] = float(cell[0])
                cell[1] = float(cell[1])
                cell[2] = float(cell[2])
                self.Gwiazdy.append(cell)
        self.ile_gwiazd = len(self.Gwiazdy)

                
    def Losuj_Gwiazdy(self,ile_gwiazd,down,up):
        self.Gwiazdy.clear()
        for i in range (ile_gwiazd):
            self.Gwiazdy.append([  round(random.uniform(down, up), 6) , round(random.uniform(down, up), 6) , round(random.uniform(down, up), 6)] )
        self.ile_gwiazd = len(self.Gwiazdy)

    def Podziel_Gwiazdy(self):
        self.Konstelacje = [[] for _ in range(8)]  # Utwórz 8 pustych list

        for gwiazda in self.Gwiazdy:
            x, y, z = gwiazda
            index = 0
            if (z > 0):
                if (x > 0) and (y > 0):
                    index = 0
                elif (x < 0) and (y > 0):
                    index = 1
                elif (x < 0) and (y < 0):
                    index = 2
                elif (x > 0) and (y < 0):
                    index = 3
            else:
                if (x > 0) and (y < 0):
                    index = 4
                elif (x < 0) and (y < 0):
                    index = 5
                elif (x < 0) and (y > 0):
                    index = 6
                elif (x > 0) and (y > 0):
                    index = 7
            self.Konstelacje[index].append(gwiazda)

        self.Konstelacje = [konstelacja for konstelacja in self.Konstelacje if konstelacja]  # Usuń puste konstelacje
            
    def Dystans(self, gwiazda1, gwiazda2):
        x1, y1, z1 = gwiazda1
        x2, y2, z2 = gwiazda2
        return m.sqrt((x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2)
            
    def Najblizsze_Gwiazdy(self):
        uzyte_gwiazdy = []

        for i in range(len(self.Konstelacje) - 1):
            min_odleglosc = None
            najblizsze_gwiazdy = None
            for gwiazda1 in self.Konstelacje[i]:
                for gwiazda2 in self.Konstelacje[i + 1]:
                    if gwiazda1 in uzyte_gwiazdy or gwiazda2 in uzyte_gwiazdy:
                        continue
                    odleglosc = self.Dystans(gwiazda1, gwiazda2)
                    if min_odleglosc is None or odleglosc < min_odleglosc:
                        min_odleglosc = odleglosc
                        najblizsze_gwiazdy = (gwiazda1, gwiazda2)                 
            if najblizsze_gwiazdy is not None:
                self.Konstelacje[i].remove(najblizsze_gwiazdy[0])
                self.Konstelacje[i].append(najblizsze_gwiazdy[0])
                self.Konstelacje[i + 1].remove(najblizsze_gwiazdy[1])
                self.Konstelacje[i + 1].insert(0, najblizsze_gwiazdy[1])
                uzyte_gwiazdy.append(najblizsze_gwiazdy[0])
                uzyte_gwiazdy.append(najblizsze_gwiazdy[1])

        min_odleglosc = None
        najblizsze_gwiazdy = None
        for gwiazda1 in self.Konstelacje[0]:
            for gwiazda2 in self.Konstelacje[-1]:
                if gwiazda1 in uzyte_gwiazdy or gwiazda2 in uzyte_gwiazdy:
                    continue
                odleglosc = self.Dystans(gwiazda1, gwiazda2)
                if min_odleglosc is None or odleglosc < min_odleglosc:
                    min_odleglosc = odleglosc
                    najblizsze_gwiazdy = (gwiazda1, gwiazda2)
        if najblizsze_gwiazdy is not None:
            self.Konstelacje[0].remove(najblizsze_gwiazdy[0])
            self.Konstelacje[0].insert(0, najblizsze_gwiazdy[0])
            self.Konstelacje[-1].remove(najblizsze_gwiazdy[1])
            self.Konstelacje[-1].append(najblizsze_gwiazdy[1])

    def Inicjalizacja_Gwiazdami(self):
        self.Populacje = [[] for _ in range(self.ile_populacji)]  
        for i in range(len(self.Populacje)):
            for j in range(self.rozmiar_populacji):
                temp = self.Gwiazdy.copy()
                if j > 0:
                    random.shuffle(temp)
                self.Populacje[i].append(temp)
        if self.nn_init_button.isChecked():
            self.Algorytm_Najblizszego_Sasiada()


    def Inicjalizacja_Konstelacjami(self):
        self.Populacje = [[] for _ in range(self.ile_populacji)]  
        for i in range(len(self.Populacje)):
            for j in range(self.rozmiar_populacji):
                temp = self.Konstelacje[i].copy()
                if j > 0:
                    mid_part = temp[1:-1]
                    random.shuffle(mid_part)
                    temp[1:-1] = mid_part
                self.Populacje[i].append(temp)
        if self.nn_init_button.isChecked():
            self.Algorytm_Najblizszego_Sasiada()

    def Algorytm_Najblizszego_Sasiada(self):
        for i in range (len(self.Populacje)):
            for j in range (len(self.Populacje[i])):
        
                if len(self.Populacje[i][j]) > 3:   
                    stars_to_visit = self.Populacje[i][j].copy()
                    self.Populacje[i][j].clear()
                    
                    if not self.alfaomega:
                        start_star = stars_to_visit[0]
                        stars_to_visit.remove(start_star)
                        self.Populacje[i][j].append(start_star)
                        
                        end_star = stars_to_visit[-1]
                        stars_to_visit.remove(end_star)

                    indeks_startowy = random.randint(0, len(stars_to_visit)-1)
                    current_star = stars_to_visit[indeks_startowy]
                    self.Populacje[i][j].append(current_star)
                    stars_to_visit.remove(current_star)

                    while len(stars_to_visit) > 0:
                        distances = [(gwiazda, m.sqrt((self.Populacje[i][j][-1][0] - gwiazda[0]) ** 2 + (self.Populacje[i][j][-1][1] - gwiazda[1]) ** 2 + (self.Populacje[i][j][-1][2] - gwiazda[2]) ** 2)) for gwiazda in stars_to_visit]
                        nearest_star, _ = min(distances, key=lambda item:item[1])
                        self.Populacje[i][j].append(nearest_star)
                        stars_to_visit.remove(nearest_star)
                        
                    if not self.alfaomega:  
                        self.Populacje[i][j].append(end_star)
            
    def Przystosowanie(self):
        self.Oceny = [[] for _ in range(len(self.Populacje))]
        for i in range(len(self.Populacje)):
            for j in range(len(self.Populacje[i])):
                ocena = sum(m.sqrt((self.Populacje[i][j][k][0]-self.Populacje[i][j][k+1][0])**2 + (self.Populacje[i][j][k][1]-self.Populacje[i][j][k+1][1])**2 + (self.Populacje[i][j][k][2]-self.Populacje[i][j][k+1][2])**2) for k in range(len(self.Populacje[i][j])-1))
                if self.alfaomega:
                    ocena += self.Dystans(self.Populacje[i][j][-1], self.Populacje[i][j][0])
                self.Oceny[i].append(ocena)


    def Sortuj_Populacje(self):
        for i in range(len(self.Populacje)):
            indeksy = np.argsort(self.Oceny[i])
            posortowana_populacja = [self.Populacje[i][j] for j in indeksy]
            posortowane_oceny = [self.Oceny[i][j] for j in indeksy]

            self.Populacje[i].clear()
            self.Populacje[i].extend(posortowana_populacja)

            self.Oceny[i].clear()
            self.Oceny[i].extend(posortowane_oceny)
            
    def Ruletka_rankingowa(self):
        for i in range(len(self.Populacje)):
            suma = sum(range(0,len(self.Populacje[i])+1))
            wagi = list(range(len(self.Populacje[i]))+np.ones(len(self.Populacje[i])))
            wagi.sort(reverse=True)
            
            self.Sortuj_Populacje()
            Szanse=[]
            
            for k in wagi:
                szansa = (k/suma)*100
                if k != wagi[0]:
                    szansa+=Szanse[len(Szanse)-1]
                Szanse.append(szansa)
                
            Los = []
            for _ in range(self.rozmiar_potomstwa):
                los = random.uniform(0, 100)
                Los.append(los)
                
            for los in Los:
                indeks = 0
                while(los>Szanse[indeks]):
                    indeks+=1
            
                self.Potomstwa[i].append(self.Populacje[i][indeks])
        
        
    def Turniejowa(self):
        for i in range(len(self.Populacje)):
            for j in range(self.rozmiar_potomstwa):
                skip = 0
                while(j>=len(self.Populacje[i])):
                    j-=len(self.Populacje[i])
                    skip += 1
                
                Turniej = []
                Noty =[]
                
                Turniej.append(self.Populacje[i][j-1-skip])
                Turniej.append(self.Populacje[i][j])
                Noty.append(self.Oceny[i][j-1-skip])
                Noty.append(self.Oceny[i][j])
                    
                szranki = random.uniform(0, 1)
                if 1 >= szranki:
                    self.Potomstwa[i].append(Turniej[Noty.index(min(Noty))])
                else:
                    self.Potomstwa[i].append(Turniej[Noty.index(max(Noty))])
    
    def Selekcja(self):
        self.Potomstwa = [[] for _ in range(len(self.Populacje))]
        if self.roulette_only_button.isChecked():
            self.Ruletka_rankingowa()
        elif self.tournament_only_button.isChecked():
            self.Turniejowa()
        elif self.selection_switch_button.isChecked():
            if self.epoka % 2 == 0:
                self.Ruletka_rankingowa()
            elif self.epoka % 2 == 1:
                self.Turniejowa()
        elif self.selection_random_button.isChecked():
            x = random.randint(0, 1)
            if x==0:
                self.Ruletka_rankingowa()
            else:
                self.Turniejowa()

    def Krzyzowanie_PMX(self):
        for i in range(len(self.Potomstwa)):
            if len(self.Potomstwa[i][0]) > 3:
                for j in range(int(len(self.Potomstwa[i])/2)):
                    if random.uniform(0, 1) <= self.p_krzyzowania:
                        parent1 = self.Potomstwa[i][2*j]
                        parent2 = self.Potomstwa[i][(2*j)+1]

                        # Losujemy dwa punkty graniczne krzyżowania
                        cutpoint1 = random.randint(0, len(parent1) - 1)
                        cutpoint2 = random.randint(cutpoint1 + 1, len(parent1))

                        # Tworzymy kopie rodziców, aby nie zmieniać ich podczas krzyżowania
                        child1 = parent1[:]
                        child2 = parent2[:]

                        # Zamieniamy fragmenty między punktami granicznymi
                        child1[cutpoint1:cutpoint2] = parent2[cutpoint1:cutpoint2]
                        child2[cutpoint1:cutpoint2] = parent1[cutpoint1:cutpoint2]

                        # Naprawiamy powtarzające się wartości w potomkach
                        for k in range(len(child1)):
                            if k < cutpoint1 or k >= cutpoint2:
                                while child1[k] in child1[cutpoint1:cutpoint2]:
                                    index = child1[cutpoint1:cutpoint2].index(child1[k])
                                    child1[k] = parent1[cutpoint1:cutpoint2][index]

                            if k < cutpoint1 or k >= cutpoint2:
                                while child2[k] in child2[cutpoint1:cutpoint2]:
                                    index = child2[cutpoint1:cutpoint2].index(child2[k])
                                    child2[k] = parent2[cutpoint1:cutpoint2][index]
                                
                        self.Potomstwa[i][2*j] = child1.copy()
                        self.Potomstwa[i][2*j+1] = child2.copy()

    def Krzyzowanie_CX(self):
        for i in range(len(self.Potomstwa)):
            if len(self.Potomstwa[i][0]) > 3:
                for j in range(int(len(self.Potomstwa[i])/2)):
                    if random.uniform(0, 1) <= self.p_krzyzowania:
                        parent1 = self.Potomstwa[i][2*j]
                        parent2 = self.Potomstwa[i][(2*j)+1]

                        n = len(parent1)
                        child1 = [None] * n
                        child2 = [None] * n

                        # Inicjalizacja tablic, które śledzą indeksy odwiedzonych elementów
                        visited1 = [False] * n
                        visited2 = [False] * n

                        # Rozpoczęcie cyklu krzyżowania CX
                        if self.alfaomega:
                            cycle_start = random.randint(0, n-1)
                        if not self.alfaomega:
                            cycle_start = random.randint(1, n-2)
                        while True:
                            # Dodajemy elementy do potomków z pierwszego rodzica
                            child1[cycle_start] = parent1[cycle_start]
                            child2[cycle_start] = parent2[cycle_start]
                            visited1[cycle_start] = True
                            visited2[cycle_start] = True

                            # Wyszukujemy indeks elementu w drugim rodzicu
                            index_in_parent2 = parent2.index(parent1[cycle_start])

                            # Przejście do kolejnego cyklu, jeśli indeks już był odwiedzony
                            if visited1[index_in_parent2]:
                                break
                            else:
                                cycle_start = index_in_parent2

                        # Wypełnienie brakujących elementów potomków
                        for k in range(n):
                            if child1[k] is None:
                                child1[k] = parent2[k]
                            if child2[k] is None:
                                child2[k] = parent1[k]
                                
                        self.Potomstwa[i][2*j] = child1.copy()
                        self.Potomstwa[i][2*j+1] = child2.copy()
                        
    def Krzyzowanie(self):
        if self.cx_only_button.isChecked():
            self.Krzyzowanie_CX()
        elif self.pmx_only_button.isChecked():
            self.Krzyzowanie_PMX()
        elif self.crossover_switch_button.isChecked():
            if self.epoka % 2 == 0:
                self.Krzyzowanie_CX()
            elif self.epoka % 2 == 1:
                self.Krzyzowanie_PMX()
        elif self.crossover_random_button.isChecked():
            x = random.randint(0, 1)
            if x==0:
                self.Krzyzowanie_CX()
            else:
                self.Krzyzowanie_PMX()
            
        
    def Mutacja(self):
        for i in range(len(self.Potomstwa)):
            if len(self.Potomstwa[i][0]) > 3:
                for j in range(len(self.Potomstwa[i])):
                    if random.uniform(0, 1) <= self.p_mutacji:
                        if self.alfaomega:
                            index1, index2 = random.sample(range(0, (len(self.Potomstwa[i][j])-1)), 2)
                        elif not self.alfaomega:
                            index1, index2 = random.sample(range(1, len(self.Potomstwa[i][j])-2), 2)
                            
                        if index1 > index2:
                            index1, index2 = index2, index1

                        if self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Mutacja dostosowana do stopnia zaawansowania rozwiązania":   
                            milestone1 = int(self.ile_gwiazd*self.rozmiar_populacji/150) # Do tego momentu mieszanie przedziału
                            milestone2 = int(self.ile_gwiazd*self.rozmiar_populacji/30) # Do tego momentu zamiana pary i przesuwanie przedziału
                                
                            if not self.alfaomega:
                                milestone1 = int(milestone1 / len(self.Konstelacje))
                                milestone2 = int(milestone2 / len(self.Konstelacje))

                            if self.epoka < milestone1:
                                x = random.randint(0, 4)
                                if x > 2:
                                    x = 0
                            elif self.epoka < milestone2:
                                x = random.randint(1, 10)
                                if x > 4 and x % 2 == 1:
                                    x = 1
                                elif x > 4 and x % 2 == 0:
                                    x = 2
                            else:
                                x = random.randint(2, 4)
                        elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Wszystkie metody mutacji losowo":
                            x = random.randint(0, 4)
                        elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Wszystkie metody mutacji na zmianę":
                            x = self.epoka % 5
                        elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez wymieszanie przedziału":
                            x = 0
                        elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez zamianę pary gwiazd":
                            x = 1
                        elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez inwersję i przesunięcie przedziału":
                            x = 2
                        elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez przestawienie jednej gwiazdy":
                            x = 3 
                        elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez inwersję przedziału":
                            x = 4
                            
                        if x == 0: # Losowy przedział
                            mid_part = self.Potomstwa[i][j][index1:index2]  
                            random.shuffle(mid_part)  
                            self.Potomstwa[i][j][index1:index2] = mid_part                           
                                                                    
                        elif x == 1: # Mutacja przez zamianę 
                            self.Potomstwa[i][j][index1], self.Potomstwa[i][j][index2] = self.Potomstwa[i][j][index2], self.Potomstwa[i][j][index1]

                        elif x == 2: # Mutacja przez inwersję i przesunięcie przedziału
                            sublist_to_reverse = self.Potomstwa[i][j][index1:index2]
                            sublist_reversed = sublist_to_reverse[::-1]
                            shift_amount = random.randint(-(index1), len(self.Potomstwa[i][j]) - index2 - 1)
                            if not self.alfaomega:
                                shift_amount = random.randint(-(index1 - 1), len(self.Potomstwa[i][j]) - index2 - 2)
                                shift_amount += index1
                                
                                
                            for star in sublist_reversed:
                                self.Potomstwa[i][j].remove(star)

                            self.Potomstwa[i][j][shift_amount:shift_amount] = sublist_reversed
                            
                        elif x == 3: # Mutacja przez wstawienie
                            gwiazda = self.Potomstwa[i][j].pop(index1)  
                            self.Potomstwa[i][j].insert(index2, gwiazda)
                            
                        elif x == 4: # Inwersja przedziału 
                            self.Potomstwa[i][j][index1:index2] = self.Potomstwa[i][j][index1:index2][::-1]

        
    def Sukcesja(self):
        for i in range(len(self.Populacje)):
            
            if self.sukcecja_calosciowa == True:
                self.Populacje[i].clear()
            self.Populacje[i].extend(self.Potomstwa[i])      
        self.Przystosowanie()
        self.Sortuj_Populacje()
        
        if self.sukcecja_calosciowa == False: 
            for j in range(len(self.Populacje)):
                self.Populacje[j] = self.Populacje[j][:self.rozmiar_populacji]
                
                
    def Migracje(self):
        ile_migracji = int(self.rozmiar_populacji/10)
        if ile_migracji < 1:
            ile_migracji = 1
        for i in range(len(self.Populacje)):
            for _ in range(ile_migracji):
                zakres = (self.epoka % len(self.Populacje))
                idx1 = random.randint(0, len(self.Populacje[i-1-zakres]) - 1)
                idx2 = random.randint(0, len(self.Populacje[i-zakres]) - 1)
                self.Populacje[i-1-zakres][idx1], self.Populacje[i-zakres][idx2] = self.Populacje[i-zakres][idx2], self.Populacje[i-1-zakres][idx1]
                self.Oceny[i-1-zakres][idx1], self.Oceny[i-zakres][idx2] = self.Oceny[i-zakres][idx2], self.Oceny[i-1-zakres][idx1]
        self.Przystosowanie()
        self.Sortuj_Populacje()     
            
            


            
    def Srednia(self):
        if self.alfaomega:
            flattened_oceny = [element for sublist in self.Oceny for element in sublist]
            self.sredni_dystans = sum(flattened_oceny) / len(flattened_oceny)
        else: 
            self.sredni_dystans = sum(sum(sublist) / len(sublist) for sublist in self.Oceny)
            for i in range(len(self.najlepsze)):
                self.sredni_dystans += self.Dystans(self.najlepsze[i][0],self.najlepsze[i-1][-1])
    
    def Zapis(self):
        self.fig_title = "NT"
        self.file_title = f"{self.ile_gwiazd}stars"
        self.fig_title += f"_{self.ile_gwiazd}stars"
        self.file_title += f"_{self.epoka}epok"
        self.fig_title += f"_{self.epoka}epok"
        self.file_title += f"_{self.ile_populacji}x{self.rozmiar_populacji}+({self.rozmiar_potomstwa})"
        self.fig_title += f"_{self.ile_populacji}x{self.rozmiar_populacji}+({self.rozmiar_potomstwa})"
        if self.none_button.isChecked():
            self.fig_title += "_kb"
            self.file_title += "_kb"
            flattened_najlepsze = self.najlepsze.copy()
        elif self.competition_button.isChecked():
            self.fig_title += "_kr"
            self.file_title += "_kr"
            flattened_najlepsze = self.najlepsze.copy()
        elif self.cooperation_button.isChecked():
            self.fig_title += "_kk"
            self.file_title += "_kk"
            flattened_najlepsze = [element for sublist in self.najlepsze for element in sublist]
            
        if self.random_init_button.isChecked():
            self.fig_title += "_random"
            self.file_title += "_random"
        elif self.nn_init_button.isChecked():
            self.fig_title += "_nn"
            self.file_title += "_nn"
        self.fig_title += ".png"
        self.file_title += ".txt"
            
        self.fig.savefig(f"zapisy/{self.fig_title}")
        with open(f"zapisy/{self.file_title}", "w") as file:
            for gwiazda in flattened_najlepsze:
                file.write(f"{gwiazda[0]} {gwiazda[1]} {gwiazda[2]}\n")
                
    def X_iteracji(self):
        for _ in range(self.iterations_box.value()):
            if self.none_button.isChecked():
                self.main_brak()
            elif self.competition_button.isChecked():
                self.main_rywalizacja()
            elif self.cooperation_button.isChecked():
                self.main_kooperacja()

    def Rysuj_Gwiazdy_brak(self):
            
        self.obecna_epoka_info = self.ax.text2D(0.02, 1, '', transform=self.ax.transAxes)
        self.najlepsza_epoka_info = self.ax.text2D(0.02, 0.95, '', transform=self.ax.transAxes)
        self.path_length_info = self.ax.text2D(0.55, 1, '', transform=self.ax.transAxes)
        self.average_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
    
        c = np.arange(1, len(self.Gwiazdy))
        norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
        self.cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.gist_rainbow)
        self.cmap.set_array([])
    
        x, y, z = zip(*self.najlepsze)
        self.punkty = self.ax.scatter(x, y, z, marker="o", c=self.cmap.to_rgba(range(len(self.najlepsze))))
        self.linie = [self.ax.plot3D([self.najlepsze[i-1][0], self.najlepsze[i][0]],
                                     [self.najlepsze[i-1][1], self.najlepsze[i][1]],
                                     [self.najlepsze[i-1][2], self.najlepsze[i][2]],
                                     c=self.cmap.to_rgba(i))[0] for i in range(len(self.najlepsze))]
        
        # Resetujemy dane dla wykresu 2D
        self.epochs = []
        self.path_lengths = []
        self.average_path_lengths = []
        self.line, = self.ax2.plot([], [], color='red', label='Przystosowanie najlepszego osobnika')  
        self.average_line, = self.ax2.plot([], [], color='green', linestyle='--', label='Średnie przystosowanie w populacji')  
        self.ax2.legend(loc='upper right')
        
    def Rysuj_Gwiazdy_rywalizacja(self):
            
        self.obecna_epoka_info = self.ax.text2D(0.02, 1, '', transform=self.ax.transAxes)
        self.najlepsza_epoka_info = self.ax.text2D(0.02, 0.95, '', transform=self.ax.transAxes)
        self.path_length_info = self.ax.text2D(0.55, 1, '', transform=self.ax.transAxes)
        self.average_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
        
        c = np.arange(1, len(self.Gwiazdy))
        norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
        self.cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.gist_rainbow)
        self.cmap.set_array([])
    
        x, y, z = zip(*self.najlepsze)
        self.punkty = self.ax.scatter(x, y, z, marker="o", c=self.cmap.to_rgba(range(len(self.najlepsze))))
        self.linie = [self.ax.plot3D([self.najlepsze[i-1][0], self.najlepsze[i][0]],
                                     [self.najlepsze[i-1][1], self.najlepsze[i][1]],
                                     [self.najlepsze[i-1][2], self.najlepsze[i][2]],
                                     c=self.cmap.to_rgba(i))[0] for i in range(len(self.najlepsze))]
        
        # Resetujemy dane dla wykresu 2D
        self.epochs = []
        self.path_lengths = []
        self.average_path_lengths = []
        self.line, = self.ax2.plot([], [], color='red', label='Przystosowanie najlepszego osobnika')  
        self.average_line, = self.ax2.plot([], [], color='green', linestyle='--', label='Średnie przystosowanie w populacjach')  
        self.ax2.legend(loc='upper right')
        
    def Rysuj_Gwiazdy_kooperacja(self):
            
        self.obecna_epoka_info = self.ax.text2D(0.02, 1, '', transform=self.ax.transAxes)
        self.najlepsza_epoka_info = self.ax.text2D(0.02, 0.95, '', transform=self.ax.transAxes)
        self.path_length_info = self.ax.text2D(0.55, 1, '', transform=self.ax.transAxes)
        self.average_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
    
        # Utwórz mapę kolorów z 8 różnymi kolorami
        cmap = plt.get_cmap('gist_rainbow')
        colors = [cmap(i) for i in np.linspace(0, 1, 8)]

        for idx, konstelacja in enumerate(self.najlepsze):
            x, y, z = zip(*konstelacja)
            color = colors[idx]
            punkty = self.ax.scatter(x, y, z, color=color)
            self.punkty.append(punkty)  # dodaj punkty do listy
            self.linie += [self.ax.plot3D([konstelacja[i-1][0], konstelacja[i][0]],
                                        [konstelacja[i-1][1], konstelacja[i][1]],
                                        [konstelacja[i-1][2], konstelacja[i][2]],
                                        color=color)[0] for i in range(1, len(konstelacja))]

        # Dodaj linie między konstelacjami
        for i in range(len(self.najlepsze)):
            last_star = self.najlepsze[i-1][-1]
            first_star = self.najlepsze[i][0]
            self.linie.append(self.ax.plot3D([last_star[0], first_star[0]],
                                        [last_star[1], first_star[1]],
                                        [last_star[2], first_star[2]], color='black')[0])

        
        # Resetujemy dane dla wykresu 2D
        self.epochs = []
        self.path_lengths = []
        self.average_path_lengths = []
        self.line, = self.ax2.plot([], [], color='red', label='Przystosowanie najlepszego osobnika')  
        self.average_line, = self.ax2.plot([], [], color='green', linestyle='--', label='Średnie przystosowanie w populacji')  
        self.ax2.legend(loc='upper right')

    def main_brak(self):
        self.epoka += 1
        
        self.Przystosowanie()
        self.Selekcja()  
        self.Krzyzowanie()
        self.Mutacja()
        self.Sukcesja() 

        
        
        # Aktualizacja najlepszego osobnika
        if self.Oceny[0][0] < self.najlepszy_dystans:
            self.najlepszy_dystans = self.Oceny[0][0]
            self.najlepszy_narodziny = self.epoka
            self.najlepsze = self.Populacje[0][0].copy()
        
        x, y, z = zip(*self.najlepsze)
        self.punkty._offsets3d = x, y, z
        for i, linia in enumerate(self.linie):
            linia.set_data_3d([self.najlepsze[i-1][0], self.najlepsze[i][0]],
                              [self.najlepsze[i-1][1], self.najlepsze[i][1]],
                              [self.najlepsze[i-1][2], self.najlepsze[i][2]])

        self.obecna_epoka_info.set_text('Aktualna epoka: {}'.format(self.epoka))
        self.najlepsza_epoka_info.set_text('Ostatnia poprawa: {}'.format(self.najlepszy_narodziny))
        self.path_length_info.set_text('Rekord populacyjny: {:.2f}'.format(self.najlepszy_dystans))
        self.Srednia()
        self.average_length_info.set_text('Średnia populacyjna: {:.2f}'.format(self.sredni_dystans))
        
        # Aktualizowanie wykresu 2D
        self.epochs.append(self.epoka)
        self.path_lengths.append(self.najlepszy_dystans)
        self.average_path_lengths.append(self.sredni_dystans)
        self.line.set_data(self.epochs, self.path_lengths)
        self.average_line.set_data(self.epochs, self.average_path_lengths)
        self.ax2.relim()
        self.ax2.autoscale_view()
        
        if (self.epoka - self.najlepszy_narodziny)!= 0 and (self.epoka - self.najlepszy_narodziny) % (20*len(self.Gwiazdy)) == 0:
            #self.stop_animation()
            self.Zapis()
        
    def main_rywalizacja(self):
        self.epoka += 1
        
        self.Przystosowanie()
        self.Selekcja()
        self.Krzyzowanie()
        self.Mutacja()
        self.Sukcesja()
        self.Migracje()
        
        
        for i in range(len(self.Populacje)):
            if self.Oceny[i][0] < self.najlepszy_dystans:
                self.najlepszy_dystans = self.Oceny[i][0]
                self.najlepszy_narodziny = self.epoka
                self.najlepsze = self.Populacje[i][0].copy()

        x, y, z = zip(*self.najlepsze)
        self.punkty._offsets3d = x, y, z
        for i, linia in enumerate(self.linie):
            linia.set_data_3d([self.najlepsze[i-1][0], self.najlepsze[i][0]],
                              [self.najlepsze[i-1][1], self.najlepsze[i][1]],
                              [self.najlepsze[i-1][2], self.najlepsze[i][2]])
            
        self.obecna_epoka_info.set_text('Aktualna epoka: {}'.format(self.epoka))
        self.najlepsza_epoka_info.set_text('Ostatnia poprawa: {}'.format(self.najlepszy_narodziny))
        self.path_length_info.set_text('Rekord populacyjny: {:.2f}'.format(self.najlepszy_dystans))
        self.Srednia()
        self.average_length_info.set_text('Średnia populacyjna: {:.2f}'.format(self.sredni_dystans))
        
        # Aktualizowanie wykresu 2D
        self.epochs.append(self.epoka)
        self.path_lengths.append(self.najlepszy_dystans)
        self.average_path_lengths.append(self.sredni_dystans)
        self.line.set_data(self.epochs, self.path_lengths)
        self.average_line.set_data(self.epochs, self.average_path_lengths)
        self.ax2.relim()
        self.ax2.autoscale_view()
        
        if (self.epoka - self.najlepszy_narodziny)!= 0 and (self.epoka - self.najlepszy_narodziny) % (20*len(self.Gwiazdy)) == 0:
            #self.stop_animation()
            self.Zapis()
        
    def main_kooperacja(self):
        self.epoka += 1

        self.Przystosowanie()
        self.Selekcja()
        self.Krzyzowanie()
        self.Mutacja()
        self.Sukcesja()  
        
        
        reprezentant = [[] for _ in range(len(self.Populacje))]
        for i in range(len(self.Populacje)):
            reprezentant[i] = self.Populacje[i][0].copy()
        flattened_reprezentant = [element for sublist in reprezentant for element in sublist]
        wynik_reprezentatna = sum(m.sqrt((flattened_reprezentant[k-1][0]-flattened_reprezentant[k][0])**2 + (flattened_reprezentant[k-1][1]-flattened_reprezentant[k][1])**2 + (flattened_reprezentant[k-1][2]-flattened_reprezentant[k][2])**2) for k in range(len(flattened_reprezentant)))
        
        if wynik_reprezentatna < self.najlepszy_dystans:
                self.najlepszy_dystans = wynik_reprezentatna
                self.najlepszy_narodziny = self.epoka
                self.najlepsze = reprezentant.copy()
                
        self.obecna_epoka_info.set_text('Aktualna epoka: {}'.format(self.epoka))
        self.najlepsza_epoka_info.set_text('Ostatnia poprawa: {}'.format(self.najlepszy_narodziny))
        self.path_length_info.set_text('Rekord populacyjny: {:.2f}'.format(self.najlepszy_dystans))
        self.Srednia()
        self.average_length_info.set_text('Średnia populacyjna: {:.2f}'.format(self.sredni_dystans))


        idx = 0
        for konstelacja in self.najlepsze:
            for i in range(1, len(konstelacja)):
                self.linie[idx].set_data_3d([konstelacja[i-1][0], konstelacja[i][0]],
                                            [konstelacja[i-1][1], konstelacja[i][1]],
                                            [konstelacja[i-1][2], konstelacja[i][2]])
                idx += 1

        # Aktualizuj linie między konstelacjami
        for i in range(len(self.najlepsze)):
            last_star = self.najlepsze[i-1][-1]
            first_star = self.najlepsze[i][0]
            self.linie[idx].set_data_3d([last_star[0], first_star[0]],
                                        [last_star[1], first_star[1]],
                                        [last_star[2], first_star[2]])
            idx += 1
            
        
        # Aktualizowanie wykresu 2D
        self.epochs.append(self.epoka)
        self.path_lengths.append(self.najlepszy_dystans)
        self.average_path_lengths.append(self.sredni_dystans)
        self.line.set_data(self.epochs, self.path_lengths)
        self.average_line.set_data(self.epochs, self.average_path_lengths)
        self.ax2.relim()
        self.ax2.autoscale_view()
        
        if (self.epoka - self.najlepszy_narodziny)!= 0 and (self.epoka - self.najlepszy_narodziny) % (20*len(self.Gwiazdy)) == 0:
            #self.stop_animation()
            self.Zapis()


aplikacja = QApplication(sys.argv)
okno = MyWindow()
okno.show()
sys.exit(aplikacja.exec_())

